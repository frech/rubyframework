require "rubyframework/version"
require "rubyframework/routing"
require "rubyframework/util"
require "rubyframework/dependencies"
require "rubyframework/controller"
require "rubyframework/file_model"

module Rubyframework
  class Application
    def call(env)
      if env['PATH_INFO'] == '/favicon.ico'
        return [404, {'Content-Type' => 'text/html'}, []]
      # elsif env['PATH_INFO'] == '/'
      #   # 第二章练习，重定向到quotes/a_quote
      #   return [301, {'Location' => 'http://localhost:3001/quotes/a_quote'}, []]
      end

      klass, act = get_controller_and_action(env)
      rack_app = get_rack_app(env)
      rack_app.call(env)
      # controller = klass.new(env)
      # text = controller.send(act)
      #
      # if controller.get_response
      #   st, hd, rs = controller.get_response.to_a
      #   [st, hd, [rs.body].flatten]
      # else
      #   controller.render(act, controller.get_instance_variables)
      #   st, hd, rs = controller.get_response.to_a
      #   [st, hd, [rs.body].flatten]
      # end
    end
  end

  class Controller
    def initialize(env)
      @env = env
    end

    def env
      @env
    end
  end
end
