class Object
  def self.const_missing(c)
    # Rails 之所以要求文件命名为下划线，类命名为驼峰，会自动加载的原因

    return nil if @calling_const_missing

    @calling_const_missing = true
    require Rubyframework.to_underscore(c.to_s)
    klass = Object.const_get(c)
    @calling_const_missing = false

    klass
  end
end
